const http = require('http');
 
http.createServer(function(req,res){
  res.writeHead(200,{
    "Content-Type" : "text/html"
  });
  res.end("Hello World");
}).listen(8000);
 

function createPyramid()
{
    let rows = 12
    for (let i = 0; i < rows; i++) {
        var output = '';
        for (let j =0; j < rows - i; j++) output += ' ';
        for (let k = 0; k <= i; k++){
            if( k % 2 == 0 ){
                output += 'X ';  
            }else{
                output += 'O ';  
            }
        }
        if(i % 2 == 0){
            console.log(output);  
        }
    } 
}
createPyramid()

console.log('Server is running at port 8000');