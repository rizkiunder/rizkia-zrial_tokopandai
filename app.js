const http = require('http');
 
http.createServer(function(req,res){
  res.writeHead(200,{
    "Content-Type" : "text/html"
  });
  res.end("Hello World");
}).listen(8000);
 

function createPyramid()
{
    let rows = 12
    for (let i = 0; i < rows; i++) {
        var output = '';
        for (let j =0; j < rows - i; j++) output += ' ';
        for (let k = 0; k <= i; k++){
            if( k % 2 == 0 ){
                output += 'X ';  
            }else{
                output += 'O ';  
            }
        }
        if(i % 2 == 0){
            console.log(output);  
        }
    } 
}
createPyramid()

function sittingArrangement(data, shaf){
    let bangku = [];
    for(let i = 0; i < data.length; i++){
        let subBangku = [];
        for(let j = 0; j < shaf; j++){
            if(data[i+j] !== undefined){
                subBangku.push(data[i+j]);
            }else{
                subBangku.push('Kursi Kosong');
            }
        }
        i++;
        bangku.push(subBangku);
    }
    console.log(bangku);
}
sittingArrangement(['dewi', 'shinta', 'ani', 'agus', 'Puji'], 3);

function mengubahHurufVokal(data){
    let vokal = ['A','I','U','E', 'O', 'a', 'i', 'u', 'e', 'o'];
    let subKata = "";
    let temp = 0;
    for(let i = 0; i < data.length; i++){
        subKataKata = '';
        let nowPush = false;
        for(let j = 0; j < vokal.length; j++){
            if(data[i] === vokal[j]){
                nowPush = true;
                if(temp === 0){
                    temp = 1;
                    subKataKata = '$';
                }else{
                    temp = 0;
                    subKataKata = '#'; 
                }
            }
        }
        if(nowPush){
            subKata += subKataKata;
        }else{
            subKata += data[i];
        }
    }
    console.log(subKata);
}
mengubahHurufVokal('i love javascript');

function soalno4()
{
    let word = 'javascript';
    let rows = '5';
    for(let i = 1; i <= word.length; i++){
        let str = '';
        
        //Add the white space to the left
        for(let k = 1; k <= word.length - i; k++){
          str += ' ';
        }
        
        //Add the '*' for each row
        for(let j = 1; j <= i; j++){
          str += switchWord(word, i);
        }
        
        //Print the pyramid pattern for each row
        console.log(str);
      }
}

function naikAngkot(data) {
    let result = [];
    let temp = [];
    temp["A"] = 0; 
    temp["B"] = 1; 
    temp["C"] = 2; 
    temp["D"] = 3; 
    temp["E"] = 4; 
    temp["F"] = 5; 

    for(let i = 0; i < data.length;  i++) {
        let hash = []
        hash["penumpang"] = data[i][0];
        hash["naikDari"] = data[i][1];
        hash["tujuan"] = data[i][2];
        hash["bayar"] = 2000 * (parseInt(temp[data[i][2]]) - parseInt(temp[data[i][1]]))
        result.push(hash)
    }
    console.log(result);
}
naikAngkot([
    ["Dimitri", "B", "F"],
    ["Icha", "A", "B"]
])

function nomor6(data){

    var totalSum = 0
    for(let i = 0; i < data.length; i++){
        for(let j = 0; j < data[i].length; j++){
            for (let z = 0; z < data[i][j].length; z++){
                totalSum = (totalSum+data[i][j][z]);
            }
        }
    }
    console.log(totalSum);
}
nomor6(
        [
            [
                [4, 5, 6],
                [9, 1, 2, 10],
                [9, 4, 3]
            ],
            [
                [4, 14, 31],
                [9, 10, 18, 12, 20],
                [1, 4, 90]
            ],
            [
                [2, 5, 10],
                [3, 4, 5],
                [2, 4, 5, 10]
            ]
        ]
    )


function switchWord(word, i){
    let str = '';
    let swap = false;
    for(let j = 0 ; j <= i; j++){
        if(j % 3 == 0){
            swap = true;
        }else{
            swap = false;
        }
        if(swap){
            str = word[word.length - j]; 
        }
    }
    return str;
}


console.log('Server is running at port 8000');